/*
 * lookup.js
 * Copyright (C) 2015 hasnaer <hasnae.rehioui@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

var Path        = require('path');


function resolveObject (object, queue, FileConfig) {
  if (queue.length === 0) {
    return object;
  }
  return resolveData(object[queue.shift()], queue, FileConfig);
}

function resolveData (data, queue, FileConfig) {
  if (data instanceof FileConfig) {
    return resolve(data, queue, FileConfig);
  } else {
    return resolveObject(data, queue, FileConfig);
  }
}

function resolve (component, queue, FileConfig) {
  if (queue.length === 0) {
    return component;
  }

  var property  = queue.shift();
  var refresh   = property.indexOf('#') === 0;
  var name      = refresh === true ? property.substring(1) : property;

  var data;
  // self
  if (name === 'inspect') {
    return component;
  } else if (name.match(/^@(value|path|leaf|format|parent|root)$/)) {
    data = component[name.substring(1)];
  } else if (name.match(/^\$(persist)$/)) {
    return component[name];
  } else {
    if (refresh === true || !(name in component.value)) {
      var root = component.root ? component.root : component;
      var path = Path.resolve(component.path, name);
      component.value[name] = new FileConfig(path, root, component);
    }
    data = component.value[name];
  }
  return resolveData(data, queue, FileConfig);
}

function find (component, property, FileConfig) {
  return resolve (component, property.split('/'), FileConfig);
}


exports.find    = find;
exports.resolve = resolve;
