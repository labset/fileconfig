/*
 * fileconfig.js
 * Copyright (C) 2015 hasnaer <hasnae.rehioui@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

require('harmony-reflect');

var Component = require('./component');
var Lookup    = require('./lookup');


function lookup (component, property) {
  return Lookup.find(component, property, FileConfig);
}

function edit (component, property, value) {
  var queue           = property.split('/');
  var targetProperty  = queue.pop();
  var targetComponent = Lookup.resolve(component, queue, FileConfig);
  targetComponent.value[targetProperty] = value;
}

function FileConfig (target, root, parent) {
  var component = Component.make(target, root, parent, FileConfig);
  return new Proxy(component, {
    get : lookup,
    set : edit
  });
}

FileConfig.global = function () {
  return new FileConfig (process.env.NODE_FILECONFIG_DIR);
};

module.exports = FileConfig;
