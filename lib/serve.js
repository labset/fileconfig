/*
 * serve.js
 * Copyright (C) 2015 hasnaer <hasnae.rehioui@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

var express     = require('express');
var serveStatic = require('serve-static');
var serveIndex  = require('serve-index');
var path        = require('path');
var util        = require('util');

var app     = module.exports = express();
var folder  = process.env.NODE_FILECONFIG_DIR; 

app.use(serveStatic(folder));
app.use(serveIndex(folder, {
  icons : true,
  view  : 'details'
}));

if (!module.parent) {
  var server = app.listen(0, function () {
    console.log(util.format('fileconfig server running on port %s',
          server.address().port));
  });
}
