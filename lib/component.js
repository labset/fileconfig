/*
 * component.js
 * Copyright (C) 2015 hasnaer <hasnae.rehioui@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

var Lookup  = require('./lookup');
var Content = require('./content');

function Component (definition, FileConfig) {
  this.path   = definition.path;
  this.root   = definition.root;
  this.parent = definition.parent;
  this.leaf   = definition.leaf;
  this.format = definition.format;
  this.value  = resolve(this.root, this.parent, definition.value, FileConfig);
}

Component.prototype.$persist = function () {
  if (this['@leaf']) {
    Content.Writers[this['@format']].write(this['@path'], this['@value']);
  }
};

function extractName (name, offset) {
  return name.substring(offset, name.length - 1);
}

function resolveString (root, parent, value, FileConfig) {
  var name;
  // resolve environment variables
  var envm = value.match(/\$\{env\..*?\}/);
  if (envm !== null) {
    name = extractName(envm[0], 6);
    return resolveString(root, parent, value.replace(envm[0], process.env[name]));
  }

  // resolve component against root
  var rootm = value.match(/^\$\{\/.*?\}$/);
  // resolve component against parent
  var parentm = value.match(/^\$\{\.\/.*?\}$/);
  // resolve component level up
  var levelupm = value.match(/^\$\{\.\.\/.*?\}$/);

  if (rootm !== null) {
    name = extractName(rootm[0], 3);
    return Lookup.find(root, name, FileConfig);
  } else if (parentm !== null) {
    name = extractName(parentm[0], 4);
    return Lookup.find(parent, name, FileConfig);
  } else if (levelupm !== null) {
    name = extractName(levelupm[0], 5);
    return Lookup.find(parent.parent, name, FileConfig);
  }
  
  return value;
}

function resolve (root, parent, value, FileConfig) {
  if (typeof value === 'string') {
    return resolveString(root, parent, value, FileConfig);
  } else {
    for (var prop in value) {
      if (value.hasOwnProperty(prop)) {
        value[prop] = resolve(root, parent, value[prop], FileConfig);
      }
    }
    return value;
  }
}


Component.make = function (target, root, parent, FileConfig) {
  return new Component(Content.define(target, root, parent), FileConfig);
};

module.exports = Component;
