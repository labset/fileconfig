/*
 * content.js
 * Copyright (C) 2015 hasnaer <hasnae.rehioui@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

var JsonFile  = require('jsonfile');
var YAML      = require('yamljs');
var FS        = require('fs');
var Path      = require('path');


var Writers = {

  json : {
    write : function (target, value) {
      JsonFile.writeFileSync(target, value);
    }
  },

  yml : {
    write : function (target, value) {
      FS.writeFileSync(target, YAML.stringify(value, 2));
    }
  }

};

var Readers = {
  dir : {
    read : function (target, root, parent) {
      if (FS.existsSync(target)) {
        var stats = FS.lstatSync(target);
        if (stats.isDirectory()) {
          var definition  = define(Path.resolve(target, 'this'), root, parent);
          var value       = definition === undefined ? {}     : definition.value;
          var format      = definition === undefined ? 'json' : definition.format;

          return {
            path    : target,
            value   : value,
            format  : format,
            leaf    : false,
            root    : root,
            parent  : parent
          };
        }
      }
    }
  },
  json : {
    read : function (target, root, parent) {
      return readFile(target, root, parent, '.json', function (filepath) {
        return JsonFile.readFileSync(filepath);
      });
    }  
  },
  yml : {
    read : function (target, root, parent) {
      return readFile (target, root, parent, '.yml', function (filepath) {
        return YAML.load(filepath);
      });
    } 
  }
};

function readFile (target, root, parent, extname, reader) {
  var filepath = Path.extname(target) === extname ? target : target + extname;
  if (FS.existsSync(filepath)) {
    return {
      path    : filepath,
      value   : reader(filepath),
      leaf    : true,
      format  : extname.substring(1),
      root    : root,
      parent  : parent
    };
  }
}

function define (target, root, parent) {
  var definition;
  for (var name in Readers) {
    if (Readers.hasOwnProperty(name)) {
      definition = Readers[name].read(target, root, parent);
      if (definition) {
        return definition;
      }
    }
  }
}

exports.define  = define;
exports.Writers = Writers;
