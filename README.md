# fileconfig

[![Join the chat at https://gitter.im/labset/fileconfig](https://badges.gitter.im/Join%20Chat.svg)](https://gitter.im/labset/fileconfig?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge)

[![NPM](https://nodei.co/npm/fileconfig.png)](https://npmjs.org/package/fileconfig)

![Travis Build Status](https://travis-ci.org/labset/fileconfig.svg?branch=master)
[![Dependency Status](https://www.versioneye.com/user/projects/5537fa837f43bcd8890003c9/badge.svg?style=flat)](https://www.versioneye.com/user/projects/5537fa837f43bcd8890003c9)
[![Code Climate](https://codeclimate.com/github/labset/fileconfig/badges/gpa.svg)](https://codeclimate.com/github/labset/fileconfig)
[![Stories in Ready](https://badge.waffle.io/labset/fileconfig.png?label=ready&title=Ready)](https://waffle.io/labset/fileconfig)


Exprimenting with configuration file loading

## Install it

```
npm install fileconfig -g
echo "export NODE_FILECONFIG_DIR=$myconfigpath" >> ~/.profile
source ~/.profile
```

## Use it

### 1. binaries

#### fileconfig

fires up a directory listing of *NODE_FILECONFIG_DIR* directory

### 2. JS library

assuming you have a configuration folder as follow :

```
+ /path/to/config/folder
  + servers
    + share
      default.yml > links to rnd/alpha.yml  
      + rnd
        alpha.yml
      + qa
        beta.yml
  + users
    hasnaer.yml
```

- servers/share/rnd/alpha.yml
```yaml
name: "Share Alpha Server"
port: 9090
dir: "${env.SHARE_DIR}"
owner : ${/users/hasnaer}
```

- it is possible to reference environment variables using ```${env.<VAR>}```
- it is possible to reference other components from root context using ```${/<COMPONENT>}```
- it is possible to reference other components from relative context using ```${./<COMPONENT>}```


Then you can fetch data as follow :

```javascript
var FileConfig  = require('fileconfig');

// if env variable NODE_FILECONFIG_DIR is set
// var config = FileConfig.global();

var config  = new FileConfig("/path/to/config/folder");

var defaultServer = config.servers.share.default;
console.log(defaultServer.name);  // out : Share Alpha Server
console.log(defaultServer.owner); // out : users.hasnaer [Object]


// if at some point name property was edited in servers/share/default.yml
// or edited at runtime as follow
defaultServer.port = 8080;
// refresh from file using # prefix
defaultServer = config.servers.share['#default'];

// you can persist/store config objects to file
defaultServer.name = "Share Aplha Server R&D";
defaultServer.$persist();

```

**Example use case**

a command line program to serve a directory listing, the directories are configured

- ~/bin/nodeHarmony

```bash
#! /usr/bin/env bash
node --harmony $@
```
*fileconfig relies on node harmony features, hence it requires script to be run with harmony flag on*

- ~/bin/serve

```bash
#! /usr/bin/env nodeHarmony

var config      = require('fileconfig').global();
var program     = require('commander');
var express     = require('express');
var serveIndex  = require('serve-index');

program
  .command('share')
  .option('-c, --client', 'select share client', 'default')
  .action(function () {
    var serverConfig = config.servers.share[program.client];
    var share = express();
    share.use(serveIndex(serverConfig.dir, { icons : true, view : 'details' }));    
    var server = share.listen(serverConfig.port, 
      function () {
        console.log(serverConfig.name + ' is running on port ' + server.address().port);
      });
    });

program.parse(process.argv);
```

Now I can run the following commands

```bash
# default
serve share
# specify client
serve share -c qa/beta
```
