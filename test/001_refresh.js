/*
 * issue_1_test.js
 * Copyright (C) 2015 hasnaer <hasnaer@haz>
 *
 * Distributed under terms of the MIT license.
 */

process.env.NODE_FILECONFIG_DIR = __dirname + '/config';
var TestConfig = require('../lib/fileconfig').global();

exports['#1.pathparam'] = function (test) {
  var share = TestConfig['server/share'];
  test.equal(share.port, 9090);
  share.port = 8080;
  test.equal(TestConfig['server/share'].port, 8080);
  test.equal(TestConfig['server/#share/port'], 9090);
  test.done();
};
