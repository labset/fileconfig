/*
 * 018_issue_test.js
 * Copyright (C) 2015 hasnaer <hasnaer@haz>
 *
 * Distributed under terms of the MIT license.
 */

process.env.NODE_FILECONFIG_DIR = __dirname + '/config';
process.env.FEMTEN  = 'wahoo 15';
process.env.HUIT    = 'wahoo 8';
process.env.DIX_HUIT = 'wahoo 18';

var TestConfig = require('../lib/fileconfig').global();

exports['#18.component-resolve-path'] = function (test) {
  var hasnaerUser = TestConfig.users.hasnaer;
  var haz = TestConfig['server/../users/hasnaer'];
  test.equals(hasnaerUser.name, haz.name);
  test.done();
};

exports['#18.component-resolve-property'] = function (test) {
  var issue8 = TestConfig.issue.eighteen;
  var dataServer = TestConfig.server.data;
  test.equals(dataServer.issue.data, process.env.DIX_HUIT);
  test.done();
};
