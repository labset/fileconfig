/*
 * 3_issue_test.js
 * Copyright (C) 2015 hasnaer <hasnaer@haz>
 *
 * Distributed under terms of the MIT license.
 */

process.env.NODE_FILECONFIG_DIR = __dirname + '/config';
var TestConfig = require('../lib/fileconfig').global();

exports['#3.ReadWrite.json'] = function (test) {
  var share = TestConfig.server.share;
  test.equal(share.port, 9090);
  share.port = 10001;
  test.equal(share.port, 10001);
  share.$persist();
  share = TestConfig.server['#share'];
  test.equals(share.port, 10001);
  share.port = 9090;
  share.$persist();
  test.done();
};

exports['#3.ReadWrite.yml'] = function (test) {
  var ftp = TestConfig.server.ftp;
  test.equal(ftp.port, 7070);
  ftp.port = 20002;
  test.equal(ftp.port, 20002);
  ftp.$persist();
  TestConfig.server['#ftp'];
  test.equal(ftp.port, 20002);
  ftp.port = 7070;
  ftp.$persist();
  test.done();
};
