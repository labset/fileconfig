/*
 * 015_issue_test.js
 * Copyright (C) 2015 hasnaer <hasnaer@haz>
 *
 * Distributed under terms of the MIT license.
 */
process.env.NODE_FILECONFIG_DIR = __dirname + '/config';
process.env.FEMTEN = 'wahoo 15';

var TestConfig = require('../lib/fileconfig').global();

exports['#15.environment-variables'] = function (test) {
  var issue15 = TestConfig.issue.fifteen;
  test.equals(issue15.data, process.env.FEMTEN);
  test.done();
};
