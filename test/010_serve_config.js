/*
 * 10_issue_test.js
 * Copyright (C) 2015 hasnaer <hasnaer@haz>
 *
 * Distributed under terms of the MIT license.
 */
process.env.NODE_FILECONFIG_DIR = __dirname + '/config';

var request = require('nodeunit-express');
var app     = require('../lib/serve');

exports['#10.serve'] = function (test) {
  var express = request(app);
  express
    .get('/server/share.json')
    .expect(function (response) {
      test.equal(response.statusCode, 200);
      test.equal(response.headers['content-type'], 'application/json');
      test.done();
      express.close();
    });
};
