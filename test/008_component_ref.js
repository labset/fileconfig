/*
 * 008_issue_test.js
 * Copyright (C) 2015 hasnaer <hasnaer@haz>
 *
 * Distributed under terms of the MIT license.
 */
process.env.NODE_FILECONFIG_DIR = __dirname + '/config';
var TestConfig = require('../lib/fileconfig').global();

process.env.HUIT = 'wahoo 8';
process.env.FEMTEN = 'wahoo 15';

exports['#8.component-reference'] = function (test) {
  var issue8 = TestConfig.issues['8'];
  test.equals(issue8.data, process.env.HUIT);
  var relatedto = issue8.relatedto;
  test.equals(relatedto.data, process.env.FEMTEN);
  test.done();
};

exports['#8.component-reference-array'] = function (test) {
  var dataServer  = TestConfig.server.data;
  var hasnaerUser = TestConfig.users.hasnaer;
  test.equal(dataServer.owners[0].name, hasnaerUser.name);
  test.equal(dataServer.owners[0].city, hasnaerUser.city);
  test.done();
};
