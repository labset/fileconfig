/*
 * basic_test.js
 * Copyright (C) 2015 hasnaer <hasnaer@haz>
 *
 * Distributed under terms of the MIT license.
 */

process.env.NODE_FILECONFIG_DIR = __dirname + '/config';

var TestConfig  = require('../lib/fileconfig').global();
var JsonFile    = require('jsonfile');
var Path        = require('path');

exports['json-server.share.port'] = function (test) {
  test.equal(TestConfig.server.share.port, 9090);
  test.done();
};

exports['yml-server.ftp.port'] = function (test) {
  test.equal(TestConfig.server.ftp.port, 7070);
  test.done();
};

exports['#5.FileConfig.Instance'] = function (test) {
  var share = TestConfig.server.share;
  test.equal(share['@value'].port, share.port);
  test.done();
};

exports['#7.inspect'] = function (test) {
  var server = TestConfig.server;
  test.equal(server.inspect, server.inspect);
  test.done();
};
